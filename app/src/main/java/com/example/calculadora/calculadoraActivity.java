package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class calculadoraActivity extends AppCompatActivity {
    private TextView lblCalculadora;
    private TextView lblUsuario;

    private TextView lblNum1;
    private TextView lblNum2;
    private TextView lblResultado;
    private EditText txtNum1;
    private EditText txtNum2;

    private Button btnSumar;
    private Button btnResta;
    private Button btnMulti;
    private Button btnDiv;
    private Button btnLimpiar;
    private Button btnRegresar;



    private Calculadora calculadora;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        btnSumar = (Button) findViewById(R.id.btnSumar);
        btnResta = (Button) findViewById(R.id.btnResta);
        btnMulti = (Button) findViewById(R.id.btnMulti);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
        calculadora = new Calculadora();

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("name");
        lblUsuario.setText("Mi nombre es: " + nombre);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();


                if(num1.matches("")) {
                    Toast.makeText(calculadoraActivity.this, "Ingrese el primer numero", Toast.LENGTH_SHORT).show();
                } else  if (num2.matches("")){
                    Toast.makeText(calculadoraActivity.this, "Ingreseel segundo numero", Toast.LENGTH_SHORT).show();
                } else {
                    calculadora.setNum1(Float.parseFloat(num1));
                    calculadora.setNum2(Float.parseFloat(num2));
                    lblResultado.setText(calculadora.suma() + "u");

                }
            }
        });
        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();


                if(num1.matches("")) {
                    Toast.makeText(calculadoraActivity.this, "Ingrese el primer numero", Toast.LENGTH_SHORT).show();
                } else  if (num2.matches("")){
                    Toast.makeText(calculadoraActivity.this, "Ingreseel segundo numero", Toast.LENGTH_SHORT).show();
                } else {
                    calculadora.setNum1(Float.parseFloat(num1));
                    calculadora.setNum2(Float.parseFloat(num2));
                    lblResultado.setText(calculadora.resta() + "u");

                }
            }
        });
        btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();


                if(num1.matches("")) {
                    Toast.makeText(calculadoraActivity.this, "Ingrese el primer numero", Toast.LENGTH_SHORT).show();
                } else  if (num2.matches("")){
                    Toast.makeText(calculadoraActivity.this, "Ingreseel segundo numero", Toast.LENGTH_SHORT).show();
                } else {
                    calculadora.setNum1(Float.parseFloat(num1));
                    calculadora.setNum2(Float.parseFloat(num2));
                    lblResultado.setText(calculadora.multiplicacion() + "u");

                }
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String num1 = txtNum1.getText().toString();
                String num2 = txtNum2.getText().toString();


                if(num1.matches("")) {
                    Toast.makeText(calculadoraActivity.this, "Ingrese el primer numero", Toast.LENGTH_SHORT).show();
                } else  if (num2.matches("")){
                    Toast.makeText(calculadoraActivity.this, "Ingreseel segundo numero", Toast.LENGTH_SHORT).show();
                } else {
                    calculadora.setNum1(Float.parseFloat(num1));
                    calculadora.setNum2(Float.parseFloat(num2));
                    lblResultado.setText(calculadora.division() + "u");

                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtNum1.setText("");
                txtNum2.setText("");
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
