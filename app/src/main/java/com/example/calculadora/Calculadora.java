package com.example.calculadora;

import java.io.Serializable;

public class Calculadora implements Serializable {
    private float Num1;
    private float Num2;
    private float res;

    public Calculadora(float num1, float num2){
        this.Num1 = num1;
        this.Num2 = num2;
    }

    public Calculadora() {

    }

    public float getNum1() {
        return Num1;
    }

    public float  getNum2() { return Num2;}


    public float setNum1(float num1) {
        Num1=num1;
        return Num1;
    }

    public float  setNum2(float num2) {
        Num2=num2;
        return Num2;
    }
    public float suma() {
        float res = this.Num1 + this.Num2;
        return res;
    }
    public float resta() {
        float res = this.Num1 - this.Num2;
        return res;
    }
    public float multiplicacion() {
        float res = this.Num1 * this.Num2;
        return res;
    }
    public float division() {
        float res = this.Num1 / this.Num2;
        return res;
    }
}







