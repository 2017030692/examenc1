package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView lblCalculadora;
    private TextView lblUsuario;
    private TextView lblContara;
    private EditText txtUsuario;
    private EditText txtContraseña;
    private Button btnIngresar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContraseña = (EditText) findViewById(R.id.txtContraseña);

        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usuario = txtUsuario.getText().toString();
                String contra = txtContraseña.getText().toString();
                String usu = getString(R.string.user);
                String con = getString(R.string.pass);

                String nom = getString(R.string.nombre);

                if (usuario.matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese  un usuario", Toast.LENGTH_SHORT).show();
                } else if(contra.matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese una contraseña", Toast.LENGTH_SHORT).show();
                } else
                if((usuario.matches(usu)) && (contra.matches(con))){
                    Intent intent = new Intent(MainActivity.this, calculadoraActivity.class);
                    intent.putExtra("name", nom);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(MainActivity.this, "Los datos son incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });

    }
}
